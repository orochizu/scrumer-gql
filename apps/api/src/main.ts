import * as express from 'express';

import { environment } from './environments/environment';
import apollo from './app/apollo/init';
import mongo from './app/mongo';

mongo();

const app = express();
const server = apollo(app);

const port = environment.port || 3333;

app.listen(port, () => {
  console.log(`\n\nListening at http://localhost:${port}${server.graphqlPath}`);
});

app.on('error', console.error);
