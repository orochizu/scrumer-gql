import { ResolverFn, ResolverParams } from '../types';

const withAuth = <TResult, TParent, TArgs>(
  params: ResolverParams<TParent, TArgs>,
  resolver: ResolverFn<TResult, TParent, TArgs>
): TResult | Promise<TResult> => {
  const [, , { user }] = params;

  if (!user) {
    throw new Error('Unauthorized');
  }

  return resolver(...params);
};

export default withAuth;
