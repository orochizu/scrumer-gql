import { Model } from 'mongoose';
import { User, UserLogin } from '@scrumer-gql/users';
import { compareSync } from 'bcrypt';
import generateToken from './generate-token';

const logIn = async (Users: Model<User>, userLogin: UserLogin) => {
  const { email, password } = userLogin;
  const user = await Users.findOne({ email }).exec();

  if (!user || !compareSync(password, user.password)) {
    throw new Error('Cannot log in');
  }

  return generateToken({ email });
};

export default logIn;
