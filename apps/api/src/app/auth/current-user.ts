import { Model } from 'mongoose';
import { verify } from 'jsonwebtoken';

import { environment } from '../../environments/environment';
import { TokenPayload } from '../types';
import { User } from '@scrumer-gql/users';

const currentUser = async (
  Users: Model<User>,
  token: string
): Promise<User> => {
  let user = null;

  try {
    const { email } = verify(token, environment.secret) as TokenPayload;
    user = await Users.findOne({ email }).exec();
  } catch (e) {
    console.warn(`Token is not provided or is invalid`);
  }

  return user;
};

export default currentUser;
