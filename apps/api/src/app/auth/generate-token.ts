import { sign } from 'jsonwebtoken';

import { environment } from '../../environments/environment';
import { Token } from '../types';

const EXPIRES_IN = 60 * 60 * 60;

const generateToken = (data): Token => ({
  token: sign(data, environment.secret, { expiresIn: EXPIRES_IN })
});

export default generateToken;
