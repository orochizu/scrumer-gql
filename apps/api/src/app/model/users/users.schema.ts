import { gql } from 'apollo-server-express';

const usersSchema = gql`
  # Users
  type User {
    id: ID
    email: String
    firstName: String
    lastName: String
    tasks(input: UserTasksFilters!, skip: Int = 0, limit: Int = 10): Tasks
  }

  type Users {
    results: [User]!
    count: Int!
  }

  type Token {
    token: String!
  }

  input CreateUserInput {
    email: String!
    firstName: String!
    lastName: String!
    password: String!
  }

  input UpdateUserInput {
    id: ID!
    email: String
    firstName: String
    lastName: String
    oldPassword: String
    newPassword: String
  }

  input LogInUserInput {
    email: String!
    password: String!
  }

  input UserFilters {
    email: String
    firstName: String
    lastName: String
  }

  input UserTasksFilters {
    title: String
    desc: String
    prio: TaskPriority
    stat: TaskStatus
  }

  extend type Query {
    me: User!
    user(id: ID!): User
    users(input: UserFilters!, skip: Int = 0, limit: Int = 10): Users
  }

  extend type Mutation {
    createUser(input: CreateUserInput!): User!
    updateUser(input: UpdateUserInput!): User!
    removeUser(id: ID!): User!

    logIn(input: LogInUserInput): Token!
  }
`;

export default usersSchema;
