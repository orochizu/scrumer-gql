import * as mongoose from 'mongoose';
import * as bcrypt from 'bcrypt';
import { Schema } from 'mongoose';

import { User } from '@scrumer-gql/users';

const UserSchema: Schema = new Schema<User>({
  email: {
    type: String,
    unique: true,
    required: true
  },
  firstName: { type: String, required: true },
  lastName: { type: String, required: true },
  password: { type: String, required: true }
});

UserSchema.pre<User>('save', async function onSave(next) {
  if (!this.isModified('password')) {
    return next();
  }

  try {
    const salt = await bcrypt.genSalt();
    this.password = await bcrypt.hash(this.password, salt);

    return next();
  } catch (e) {
    return next(e);
  }
});

const Users = mongoose.model<User>('User', UserSchema);

export default Users;
