import { Model } from 'mongoose';

import { User, UserCreate, UserUpdate, UserFilters } from '@scrumer-gql/users';
import { FindManyResults } from '@scrumer-gql/api-interfaces';

export const create = async (
  Users: Model<User>,
  userCreate: UserCreate
): Promise<User> => {
  const userExists = await Users.exists({ email: userCreate.email });

  if (userExists) {
    throw new Error(`User with email ${userCreate.email} already exists`);
  }

  return Users.create(userCreate);
};

export const remove = async (Users: Model<User>, id: string): Promise<User> => {
  const user = await Users.findById(id).exec();
  Users.deleteOne({ _id: id }).exec();

  return user;
};

export const update = async (
  Users: Model<User>,
  userUpdate: UserUpdate
): Promise<User> => {
  const { id } = userUpdate;
  await Users.updateOne({ _id: id }, userUpdate).exec();

  return findById(Users, id);
};

export const findById = (Users: Model<User>, id: string): Promise<User> => {
  return Users.findById(id).exec();
};

export const findManyByFilters = async (
  Users: Model<User>,
  userFilters: UserFilters,
  skip: number,
  limit: number
): Promise<FindManyResults<User>> => {
  const { email, firstName, lastName } = userFilters;

  const results = Users.find({
    ...(email ? { email: new RegExp(email) } : {}),
    ...(lastName ? { lastName: new RegExp(lastName, 'i') } : {}),
    ...(firstName ? { firstName: new RegExp(firstName, 'i') } : {})
  })
    .skip(skip)
    .limit(limit)
    .exec();

  const count = Users.countDocuments().exec();

  return {
    results,
    count
  };
};
