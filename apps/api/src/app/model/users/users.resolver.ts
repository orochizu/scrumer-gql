import logIn from '../../auth/log-in';
import withAuth from '../../auth/with-auth';

import { ResolverParams } from '../../types';
import {
  User,
  UserLogin,
  UserUpdate,
  UserCreate,
  UserFilters,
  UserTasksFilters
} from '@scrumer-gql/users';

import * as TasksQuery from '../tasks/tasks.query';
import * as UsersQuery from './users.query';

const usersResolver = {
  Query: {
    me: (...params: ResolverParams) =>
      withAuth(params, (_, __, context) => {
        const { user } = context;
        return user;
      }),

    user: (...params: ResolverParams<{ id: string }>) =>
      withAuth(params, (_, args, context) => {
        const { id } = args;
        const { Users } = context.models;

        return UsersQuery.findById(Users, id);
      }),

    users: (
      ...params: ResolverParams<{
        input: UserFilters;
        skip: number;
        limit: number;
      }>
    ) =>
      withAuth(params, (_, args, context) => {
        const { input, skip, limit } = args;
        const { Users } = context.models;

        return UsersQuery.findManyByFilters(Users, input, skip, limit);
      })
  },
  Mutation: {
    updateUser: (...params: ResolverParams<{ input: UserUpdate }>) =>
      withAuth(params, (_, args, context) => {
        const { input } = args;
        const { Users } = context.models;

        return UsersQuery.update(Users, input);
      }),

    removeUser: (...params: ResolverParams<{ id: string }>) =>
      withAuth(params, (_, args, context) => {
        const { id } = args;
        const { Users } = context.models;

        return UsersQuery.remove(Users, id);
      }),

    createUser: (...params: ResolverParams<{ input: UserCreate }>) => {
      const [, args, context] = params;

      const { input } = args;
      const { Users } = context.models;

      return UsersQuery.create(Users, input);
    },

    logIn: (...params: ResolverParams<{ input: UserLogin }>) => {
      const [, args, context] = params;

      const { input } = args;
      const { Users } = context.models;

      return logIn(Users, input);
    }
  },
  User: {
    id: (user: User) => user._id,

    tasks: (
      ...params: ResolverParams<
        {
          input: UserTasksFilters;
          skip: number;
          limit: number;
        },
        User
      >
    ) => {
      const [user, args, context] = params;

      const { input, skip, limit } = args;
      const { _id: id } = user;

      const { Tasks } = context.models;

      return TasksQuery.findManyByFilters(
        Tasks,
        {
          ...input,
          assigneeId: id,
          reporterId: id
        },
        skip,
        limit
      );
    }
  }
};

export default usersResolver;
