import { gql } from 'apollo-server-express';

const tasksSchema = gql`
  # Tasks
  enum TaskPriority {
    TRIVIAL
    MINOR
    NORMAL
    MAJOR
    CRITICAL
  }

  enum TaskStatus {
    TO_DO
    IN_PROGRESS
    DONE
    REJECTED
  }

  type Task {
    id: ID
    title: String
    desc: String
    prio: TaskPriority
    stat: TaskStatus
    assignee: User
    reporter: User
    createdAt: String
    updatedAt: String
  }

  type Tasks {
    results: [Task]!
    count: Int!
  }

  input CreateTaskInput {
    title: String!
    desc: String!
    prio: TaskPriority!
    stat: TaskStatus!
    assigneeId: ID
  }

  input UpdateTaskInput {
    id: ID!
    title: String
    desc: String
    prio: TaskPriority
    stat: TaskStatus
    assigneeId: ID
  }

  input TaskFilters {
    title: String
    desc: String
    prio: TaskPriority
    stat: TaskStatus
    assigneeId: ID
    reporterId: ID
  }

  extend type Query {
    task(id: ID!): Task
    tasks(input: TaskFilters!, skip: Int = 0, limit: Int = 10): Tasks
  }

  extend type Mutation {
    createTask(input: CreateTaskInput!): Task!
    updateTask(input: UpdateTaskInput!): Task!
    removeTask(id: ID!): Task!
  }
`;

export default tasksSchema;
