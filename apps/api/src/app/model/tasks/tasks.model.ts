import * as mongoose from 'mongoose';
import { Schema } from 'mongoose';

import { TaskPriority } from '@scrumer-gql/enums';
import { TaskStatus } from '@scrumer-gql/enums';
import { Task } from '@scrumer-gql/tasks';

const PRIORITIES = Object.values(TaskPriority);
const STATUSES = Object.values(TaskStatus);

const TaskSchema: Schema = new Schema<Task>(
  {
    title: { type: String, required: true },
    desc: { type: String, required: true },
    prio: { type: String, enum: PRIORITIES, required: true },
    stat: { type: String, enum: STATUSES, required: true },
    reporterId: { type: String, required: true },
    assigneeId: { type: String }
  },
  {
    timestamps: {
      createdAt: true,
      updatedAt: true
    }
  }
);

const Tasks = mongoose.model<Task>('Task', TaskSchema);

export default Tasks;
