import { Model } from 'mongoose';
import { Task, TaskCreate, TaskFilters, TaskUpdate } from '@scrumer-gql/tasks';
import { FindManyResults } from '@scrumer-gql/api-interfaces';

export const create = (
  Tasks: Model<Task>,
  taskCreate: TaskCreate
): Promise<Task> => {
  return Tasks.create(taskCreate);
};

export const remove = async (Tasks: Model<Task>, id: string): Promise<Task> => {
  const task = await Tasks.findById(id).exec();
  Tasks.deleteOne({ _id: id }).exec();

  return task;
};

export const update = async (
  Tasks: Model<Task>,
  taskUpdate: TaskUpdate
): Promise<Task> => {
  const { id } = taskUpdate;
  await Tasks.updateOne({ _id: id }, taskUpdate).exec();

  return findById(Tasks, id);
};

export const findById = (Tasks: Model<Task>, id: string): Promise<Task> => {
  return Tasks.findById(id).exec();
};

export const findManyByFilters = async (
  Tasks: Model<Task>,
  taskFilters: TaskFilters,
  skip: number,
  limit: number
): Promise<FindManyResults<Task>> => {
  const { title, desc, prio, stat, reporterId, assigneeId } = taskFilters;

  const results = Tasks.find({
    ...(desc ? { desc: new RegExp(desc, 'i') } : {}),
    ...(title ? { title: new RegExp(title, 'i') } : {}),

    $or: [reporterId ? { reporterId } : {}, assigneeId ? { assigneeId } : {}],

    ...(prio ? { prio } : {}),
    ...(stat ? { stat } : {})
  })
    .skip(skip)
    .limit(limit)
    .exec();

  const count = Tasks.countDocuments().exec();

  return {
    results,
    count
  };
};
