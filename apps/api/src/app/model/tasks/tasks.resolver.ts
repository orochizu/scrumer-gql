import withAuth from '../../auth/with-auth';

import { Task, TaskCreate, TaskFilters, TaskUpdate } from '@scrumer-gql/tasks';
import { ResolverParams } from '../../types';
import * as TasksQuery from './tasks.query';
import * as UsersQuery from '../users/users.query';

const tasksResolver = {
  Query: {
    task: (...params: ResolverParams<{ id: string }>) =>
      withAuth(params, (_, args, context) => {
        const { id } = args;
        const { Tasks } = context.models;

        return TasksQuery.findById(Tasks, id);
      }),

    tasks: (
      ...params: ResolverParams<{
        input: TaskFilters;
        skip: number;
        limit: number;
      }>
    ) =>
      withAuth(params, (_, args, context) => {
        const { input, skip, limit } = args;
        const { Tasks } = context.models;

        return TasksQuery.findManyByFilters(Tasks, input, skip, limit);
      })
  },
  Mutation: {
    createTask: (...params: ResolverParams<{ input: TaskCreate }>) =>
      withAuth(params, (_, args, context) => {
        const { input } = args;

        const { user } = context;
        const { Tasks } = context.models;

        return TasksQuery.create(Tasks, { ...input, reporterId: user._id });
      }),

    updateTask: (...params: ResolverParams<{ input: TaskUpdate }>) =>
      withAuth(params, (_, args, context) => {
        const { input } = args;
        const { Tasks } = context.models;

        return TasksQuery.update(Tasks, input);
      }),

    removeTask: (...params: ResolverParams<{ id: string }>) =>
      withAuth(params, (_, args, context) => {
        const { id } = args;
        const { Tasks } = context.models;

        return TasksQuery.remove(Tasks, id);
      })
  },
  Task: {
    assignee: (...params: ResolverParams<{}, Task>) => {
      const [task, , context] = params;
      const { Users } = context.models;

      return UsersQuery.findById(Users, task.assigneeId);
    },

    reporter: (...params: ResolverParams<{}, Task>) => {
      const [task, , context] = params;
      const { Users } = context.models;

      return UsersQuery.findById(Users, task.reporterId);
    }
  }
};

export default tasksResolver;
