import usersResolver from '../model/users/users.resolver';
import tasksResolver from '../model/tasks/tasks.resolver';

export default [usersResolver, tasksResolver];
