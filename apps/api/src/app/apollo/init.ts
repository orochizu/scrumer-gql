import { Express } from 'express';
import { ApolloServer } from 'apollo-server-express';

import typeDefs from './type-defs';
import context from './context';
import resolvers from './resolvers';

const apollo = (app: Express): ApolloServer => {
  const server = new ApolloServer({ typeDefs, resolvers, context });
  server.applyMiddleware({ app });

  return server;
};

export default apollo;
