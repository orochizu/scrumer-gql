import { Request } from 'express';

import currentUser from '../auth/current-user';

import Users from '../model/users/users.model';
import Tasks from '../model/tasks/tasks.model';

import { Context } from '../types';
import { Models } from '../types';

const AUTH_HEADER = 'authorization';

const MODELS: Models = {
  Users,
  Tasks
};

const context = async ({ req }: { req: Request }): Promise<Context> => {
  const token = (req.headers[AUTH_HEADER] || '') as string;

  const user = await currentUser(Users, token);

  return {
    user,
    models: MODELS
  };
};

export default context;
