import { gql } from 'apollo-server-express';

import tasksSchema from '../model/tasks/tasks.schema';
import usersSchema from '../model/users/users.schema';

const rootSchema = gql`
  type Query
  type Mutation

  schema {
    query: Query
    mutation: Mutation
  }
`;

export default [rootSchema, tasksSchema, usersSchema];
