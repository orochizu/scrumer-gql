import * as mongoose from 'mongoose';
import { environment } from '../../environments/environment';

const mongo = () => {
  mongoose
    .connect(
      `mongodb://${environment.database.host}:${environment.database.port}/${environment.database.name}`,
      {
        useCreateIndex: true,
        useNewUrlParser: true,
        useUnifiedTopology: true,
      }
    )
    .then(() => console.log('Connected to MongoDB'))
    .catch(console.error);
};

export default mongo;
