import { Model } from 'mongoose';
import { User } from '@scrumer-gql/users';
import { Task } from '@scrumer-gql/tasks';

export interface Models {
  Users: Model<User>;
  Tasks: Model<Task>;
}
