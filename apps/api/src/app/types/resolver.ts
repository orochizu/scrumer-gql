import { GraphQLResolveInfo } from 'graphql';
import { Context } from './context';

export type ResolverParams<TArgs = {}, TParent = {}> = [
  TParent,
  TArgs,
  Context,
  GraphQLResolveInfo
];
export type ResolverFn<TResult, TArgs = {}, TParent = {}> = (
  parent: TParent,
  args: TArgs,
  context: Context,
  info?: GraphQLResolveInfo
) => TResult | Promise<TResult>;
