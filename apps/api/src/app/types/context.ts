import { Models } from './models';
import { User } from '@scrumer-gql/users';

export interface Context {
  user: User;
  models: Models;
}
