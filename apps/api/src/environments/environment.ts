export const environment = {
  production: false,
  port: 3333,
  secret: 'SUPER_SECRET_KEY',
  database: {
    host: '127.0.0.1',
    port: 27017,
    name: 'scrumer',
    username: 'root',
    password: 'root'
  }
};
