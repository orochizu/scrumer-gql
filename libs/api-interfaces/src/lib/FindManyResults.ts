export interface FindManyResults<T> {
  results: T[] | Promise<T[]>;
  count: number | Promise<number>;
}
