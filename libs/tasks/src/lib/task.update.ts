import { TaskPriority, TaskStatus } from '@scrumer-gql/enums';

export interface TaskUpdate {
  id: string;
  title?: string;
  desc?: string;
  prio?: TaskPriority;
  stat?: TaskStatus;
  assigneeId?: string;
}
