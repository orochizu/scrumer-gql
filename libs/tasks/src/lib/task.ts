import { Document } from 'mongoose';

import { TaskPriority, TaskStatus } from '@scrumer-gql/enums';

export interface Task extends Document {
  title: string;
  desc: string;
  prio: TaskPriority;
  stat: TaskStatus;
  assigneeId: string;
  reporterId: string;
  createdAt: Date;
  updatedAt: Date;
}
