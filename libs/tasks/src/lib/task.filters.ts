import { TaskPriority, TaskStatus } from '@scrumer-gql/enums';

export interface TaskFilters {
  title: string;
  desc: string;
  prio: TaskPriority;
  stat: TaskStatus;
  reporterId: string;
  assigneeId: string;
}
