import { TaskPriority, TaskStatus } from '@scrumer-gql/enums';

export interface UserTasksFilters {
  title: string;
  desc: string;
  prio: TaskPriority;
  stat: TaskStatus;
}
