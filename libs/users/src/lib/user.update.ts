export interface UserUpdate {
  id: string;
  email?: string;
  firstName?: string;
  lastName?: string;
  oldPassword?: string;
  newPassword?: string;
}
