export interface UserFilters {
  email?: string;
  firstName?: string;
  lastName?: string;
}
