export interface UserCreate {
  email: string;
  firstName: string;
  lastName: string;
  password: string;
}
